package com.classpath.springbootdemo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("/api/health")
@RequiredArgsConstructor
@Slf4j
public class ApplicationProbeController {

    private final ApplicationAvailability applicationAvailability;
    private final ApplicationEventPublisher eventPublisher;

    @PostMapping("/liveness")
    public void updateLivenessState(){
        LivenessState livenessState = this.applicationAvailability.getLivenessState();
        LivenessState currentLivenessState = livenessState == LivenessState.BROKEN ? LivenessState.CORRECT : LivenessState.BROKEN;
        Object state = currentLivenessState == LivenessState.CORRECT ? " System is functional " : new IllegalStateException("Application is not healthy");
        AvailabilityChangeEvent.publish(this.eventPublisher, state, currentLivenessState);
        log.info(" Updated Liveness state :: {}", applicationAvailability.getLivenessState());
    }
}
