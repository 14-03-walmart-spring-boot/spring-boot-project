package com.classpath.springbootdemo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/*
@Configuration
@Slf4j
*/
public class WebApplicationSecurityConfiguration  {//extends WebSecurityConfigurerAdapter {

    /*@Override
    //Authentication
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .inMemoryAuthentication()
                    .withUser("kiran")
                    .password(passwordEncoder().encode("welcome"))
                    .roles("USER")
                .and()
                    .withUser("vinay")
                    .password(passwordEncoder().encode("welcome"))
                    .roles("USER", "ADMIN")
                .and()
                    .withUser("shashi")
                    .password(passwordEncoder().encode("welcome"))
                    .roles("USER", "ADMIN", "SUPER_ADMIN");

    }

    @Override
    //Authorization
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().disable();
        http
                .authorizeRequests()
                    .antMatchers("/login**", "/h2-console**", "/logout**")
                        .permitAll()
                    .antMatchers(HttpMethod.GET, "/api/v1/orders**")
                        .hasAnyRole("USER", "ADMIN", "SUPER_ADMIN")
                    .antMatchers(HttpMethod.POST, "/api/v1/orders**")
                        .hasAnyRole("ADMIN", "SUPER_ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/api/v1/orders**")
                        .hasRole("SUPER_ADMIN")
                    .anyRequest()
                        .fullyAuthenticated()
*//*
                    .and()
                        .formLogin()
*//*
                    .and()
                        .httpBasic()
                    .and()
                        .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }*/
}
