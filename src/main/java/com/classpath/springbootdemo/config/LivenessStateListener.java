package com.classpath.springbootdemo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LivenessStateListener {


    @EventListener
    public void onLivenessStateChage(AvailabilityChangeEvent<LivenessState> event){
        LivenessState livenessState = event.getState();
        if (livenessState == LivenessState.BROKEN){
            log.error("Liveness probe failed :: {}", event.getSource());
        } else {
            log.info("Liveness probe successfull :: {}", event.getSource());
        }

    }
}
