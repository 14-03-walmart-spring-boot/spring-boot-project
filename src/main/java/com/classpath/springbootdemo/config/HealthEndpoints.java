package com.classpath.springbootdemo.config;

import com.classpath.springbootdemo.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
class DBHealthCheckEndpoint implements HealthIndicator{

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        try {
            long count = this.orderRepository.count();
            log.info("No of records from orders table :: {} ", count);
            return Health.up().withDetail("DB Service", " DB service is up").build();
        }catch (Exception exception){
            log.error("Unable to connect to DB :: {}", exception.getMessage());
        }
        return Health.status(Status.DOWN).withDetail("DB Service", "DB service is down").build();
    }
}
@Component
@RequiredArgsConstructor
@Slf4j
class PaymentGatewayHealthCheckEndpoint implements HealthIndicator{

    @Override
    public Health health() {
            return Health.up().withDetail("Payment Gateway Service", " Payment Gateway service is up").build();
    }
}